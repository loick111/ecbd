PDF = xelatex
MODE = --interaction batchmode

cours: cours.tex
	$(PDF) $(MODE) cours.tex
	#rm *.out *.aux *.log

clean:
	#rm *.pdf
	rm *.out *.aux *.log *.toc
